const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const Db = require('./database/db');
const UserRouter = require('./routers/userRouter');
const session = require('./middlewares/session');


class Server{

    constructor(){
        this.db = new Db();
        this.app = express();
        this.#config();
    }

    #config(){
        this.app.use(express.json());
        this.app.use(cors());
        this.app.use(morgan('tiny'));
        this.app.get('/', (req, res)=>{
            res.status(200).json({message: 'All ok'});
        });
        this.app.use(session);
        let router = new UserRouter();
        this.app.use(router.router);
        this.app.listen(3000, ()=>{
            console.log("Corriendo en el puerto 3000");
        })
    }

}

new Server();