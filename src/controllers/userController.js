const User = require('../models/user');

class UserController{

    register(req, res){
        const {email, password} = req.body;
        User.create({email, password}, (error, doc)=>{
            if(error){
                res.status(500).json({error});
            }else{
                req.session.userId = doc._id;
                req.session.email = email;
                res.status(200).json({message: 'OK'});
            }
        })
    }

    login(req, res){
        if(req.session.userId != undefined && req.session.userId != null){
            res.status(200).json({email: req.session.email});
        }else{
            res.status(200).json({email: 'none'});
        }
    }

    logout(req, res){
        req.session.destroy();
    }

}

module.exports = UserController;