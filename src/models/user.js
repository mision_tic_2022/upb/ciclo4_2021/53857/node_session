const {Schema, model} = require('mongoose');

const userSchema = new Schema({
    email:{
        type: String
    },
    password:{
        type: String
    }
},{
    collection: 'users'
});

module.exports = model('User', userSchema);