const {Router} = require('express');
const UserController = require('../controllers/userController');

class UserRouter{

    constructor(){
        this.router = Router();
        this.#config();
    }

    #config(){
        const objUser = new UserController();
        this.router.post('/user', objUser.register);
        this.router.get('/login', objUser.login);
    }
}

module.exports = UserRouter;