const session = require('express-session');
const MongoStore = require('connect-mongo');

module.exports = session({
    secret: 'estoesunaCONTRASEÑAsuperSEGURA1234',
    resave: false,
    saveUninitialized: true,
    store: MongoStore.create({mongoUrl: 'mongodb://localhost:27017/sesiones_53857'}),
    name: 'SESION_MINTIC',
    cookie:{
        httpOnly: true,
        maxAge: 60000
    }
});